from numba import njit
import numpy as np

@njit
def compressor_performance(to, tc):
    """
    Calculates the performance of a compressor based on the temperature of the evaporator and condenser.

    ************************************************************										
    BITZER Software v7.0.0 rev0										
    (c) 2024, BITZER, Germany. All data subject to change.										
    Samstag, 13. Januar 2024 14:50:05										
    ************************************************************										
                                            
    Compressor Selection: Semi-hermetic Reciprocating Compressors										
    ------------------------------------------------------------										
    Input Values:										
                                            
    Compressor model			2KES-05Y							
    Mode			Refrigeration and Air conditioning							
    Refrigerant			R1234ze							
    Reference temperature			Dew point temp.							
    Evaporating SST			0 °C							
    Condensing SDT			40,0 °C							
    Liq. subc. (in condenser)			3,00 K							
    Suct. gas superheat			6,00 K							
    Operating mode			Auto							
    Power supply			400V-3-50Hz							
    Useful superheat			6,00 K							
                                            
    ------------------------------------------------------------										
    Polynomial:										
    y = c1 + c2*to + c3*tc + c4*to^2 + c5*to*tc + c6*tc^2 + c7*to^3 + c8*tc*to^2 + c9*to*tc^2 + c10*tc^3										
                                            
    Coefficients:										
        c1	c2	c3	c4	c5	c6	c7	c8	c9	c10
    Q [W]	2442,185065	97,81720417	-33,7382288	1,471418118	-0,757294258	0,185603432	0,008181067	-0,009150894	0,000124483	-0,000637469
    P [W]	274,2538995	0,742057122	1,984480714	0,031868473	0,091705478	0,087140306	0,001896907	-0,00172507	0,001290996	-0,000681946
    m [kg/h]	46,23992029	1,695715316	-0,351409893	0,021701419	-0,003571017	0,002111707	0,000142444	-1,09303E-05	1,92808E-05	-6,83661E-06
    I [A]	1,50445142	-0,00028395	-0,001605938	-5,94538E-05	4,19981E-05	9,84351E-05	-4,99716E-07	1,63563E-06	1,57996E-06	-6,13072E-07

    """
    Q = 2442.185065 + 97.81720417 * to + -33.7382288 * tc + 1.471418118 * to**2 + -0.757294258 * to*tc + 0.185603432 * tc**2 + 0.008181067 * to**3 - 0.009150894 * tc*to**2 + 0.000124483 * to*tc**2 + -0.000637469 * tc**3
    P = 274.2538995 + 0.742057122 * to + 1.984480714 * tc + 0.031868473 * to**2 + 0.091705478 * to*tc + 0.087140306 * tc**2 + 0.001896907 * to**3 + -0.00172507 * tc*to**2 + 0.001290996 * to*tc**2 + -0.000681946 * tc**3
    m = 46.23992029 + 1.695715316 * to + -0.351409893 * tc + 0.021701419 * to**2 + -0.003571017 * to*tc + 0.002111707 * tc**2 + 0.000142444 * to**3 + -1.09303E-05 * tc*to**2 + 1.92808E-05 * to*tc**2 + -6.83661E-06 * tc**3
    I = 1.50445142 + -0.00028395 * to + -0.001605938 * tc + -5.94538E-05 * to**2 + 4.19981E-05 * to*tc + 9.84351E-05 * tc**2 + -4.99716E-07 * to**3 + 1.63563E-06 * tc*to**2 + 1.57996E-06 * to*tc**2 + -6.13072E-07 * tc**3
    
    return Q, P, m, I


def test_compressor():
    T_evap = 0
    T_cond = 40
    Q, P, m, I = compressor_performance(T_evap, T_cond)
    
    # Check Q is approximately 1348 W
    assert abs(Q - 1348) < 1

    # Check P is approximately 449 W
    assert abs(P - 449) < 1

    # Check m is approximately 35.1 kg/h
    assert abs(m - 35.1) < 0.1

    # Check I is approximately 1.55 A
    assert abs(I - 1.55) < 0.01

@njit
def is_point_inside_polygon(points, point):
    """
    Check if a point is inside a polygon.

    Args:
    points (list of tuples): The polygon points as (T_e, T_c).
    point (tuple): The point to check as (T_e, T_c).

    Returns:
    bool: True if the point is inside the polygon, False otherwise.
    """

    n = len(points)
    if n < 3:
        return False  # A polygon must have at least 3 points

    # Extending the list with the first point at the end

    inside = False

    x = point[0]
    y = point[1]
    for i in range(n):
        x1 = points[i][0]
        y1 = points[i][1]
        x2 = points[i + 1][0]
        y2 = points[i + 1][1]

        # Check if the point is inside the polygon
        if ((y1 > y) != (y2 > y)) and (x < (x2 - x1) * (y - y1) / (y2 - y1) + x1):
            inside = not inside

    return inside

@njit
def in_operating_range(T_evap, T_cond):
    operating_map = np.array([
            [-15, 12],
            [13, 31],
            [35, 55],
            [35, 96],
            [-5, 96],
            [-15, 81],
            [-15, 12]
        ])  # Operating Map


    return is_point_inside_polygon(operating_map, (T_evap, T_cond))

def test_in_operating_range():
    print(in_operating_range(8, 8))

if __name__ == "__main__":
    test_compressor()
    test_in_operating_range()
