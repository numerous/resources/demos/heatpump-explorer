from time import time
import numpy as np
from numba import njit
from compressor import compressor_performance
from compressor import in_operating_range
from update_svg import update_svg, read_svg, root_to_string, get_file_path

@njit
def heatpump_performance(T_source_inlet=0, T_source_outlet=-5, T_supply_inlet=40, T_supply_outlet=50, T_super = 6, T_sub_cond = 3, n_plates=25, frequency=50):
    
    T_evap_guess = T_source_inlet - T_super - 10
    T_cond_guess = T_supply_outlet + 10

    #if not in_operating_range(T_evap_guess, T_cond_guess):
        #print(T_evap_guess, T_cond_guess)
        #raise ValueError("Initial guess is not in operating range")
    
    converged = False
    max_iter = 1000000

    iteration = 0
    while iteration < max_iter:
        iteration += 1
        
        Q, P_e, m, I = compressor_performance(T_evap_guess, T_cond_guess)
        Q *= frequency/50/1000# kW
        P_e *= frequency/50/1000# kW
        m *= frequency/50/3.6# kg/h
        Q = max(1e-8, Q    ) # kW
        P_e = max(1e-6, P_e) # kW
        m = max(0, m) # kg/h
        COP = Q / P_e + 1
        
        # Evaporator
        T_source_out_ = max(T_evap_guess+0.01, T_source_outlet)
        dT1 = T_source_inlet - (T_evap_guess + T_super)
        dT2 = T_source_out_ - T_evap_guess

        if dT1 == dT2:
            LMTD = dT1
        else:
            LMTD = (dT1 - dT2) / (np.log(dT1/dT2)) # K


        A = 0.003 * n_plates # m2
        U = 4 # kW/K/m2
        P = LMTD * A * U # kW

        err = P - Q
        T_evap_next = err * 1e-4 + T_evap_guess
        change = T_evap_next - T_evap_guess


        # Update guess
        T_evap_guess = T_evap_next

        # Condenser
        T_supply_outlet_ = T_supply_outlet
        dT1_c = T_cond_guess - T_supply_outlet_
        dT2_c = T_cond_guess - T_sub_cond- T_supply_inlet

        if dT1_c == dT2_c:
            LMTD_c = dT1_c
        else:
            if dT1_c < 0:
                LMTD_c = (dT1_c + dT2_c) / 2
            else:
                LMTD_c = (dT1_c- dT2_c) / (np.log(dT1_c/dT2_c))

        A_c = 0.001 * n_plates
        U_c = 4
        P_c = LMTD_c * A_c * U_c

        err_c = P_c - Q - P_e
        T_cond_next = -err_c * 1e-4 + T_cond_guess
        change_c = T_cond_next - T_cond_guess


        if (abs(change) < 0.00001) and (abs(change_c) < 0.00001):
            converged = True
            break

        # Update guess
        T_cond_guess = T_cond_next

    
    return T_evap_guess, T_source_inlet, T_source_out_, T_cond_guess, T_supply_inlet, T_supply_outlet_, Q, P_e, COP, P, m, I, converged, iteration

svg_data, root = read_svg(get_file_path('heatpump-illustration.svg'))

def as_html(results):

    update_svg(svg_data, {
        't_cond': f'{results["T_cond"]:.1f}°C',
        't_supply_in': f'{results["T_supply_in"]:.1f}°C',
        't_supply_out': f'{results["T_supply_out"]:.1f}°C',
        't_evap': f'{results["T_evap"]:.1f}°C',
        't_source_in': f'{results["T_source_in"]:.1f}°C',
        't_source_out': f'{results["T_source_out"]:.1f}°C',
        'cop_heat': f'{results["COP"]:.1f}',
        'p_source': f'{results["Q_e"]:.2f}\u2009kW',
        'p_supply': f'{results["Q"]:.2f}\u2009kW',
        'f_ref': f'{results["m_ref"]:.0f}\u2009kg/hr',
        'p_elec': f'{results["P_e"]:.2f}\u2009kW',
        'freq_comp': f'{results["freq"]:.1f}\u2009Hz',
        'f_source': f'{results["F_source"]:.2f}\u2009L/s',
        'f_supply': f'{results["F_supply"]:.2f}\u2009L/s',

        })
    svg_content = root_to_string(root)
    # formats the results in an html string and returns it
    return f"""
    <div style="background-color: #ffffff; padding: 10px; border-radius: 10px;">
            {svg_content}
    </div>
    """


def performance_as_html(T_source_inlet=0, T_source_outlet=-5, T_supply_inlet=40, T_supply_outlet=50, T_super = 6, T_sub_cond = 3, n_plates=25, frequency=50):
    T_evap_guess, T_source_inlet, T_source_outlet, T_cond_guess, T_supply_inlet, T_supply_outlet, Q, P_e, COP, P, m, I, converged, iteration = heatpump_performance(T_source_inlet, T_source_outlet, T_supply_inlet, T_supply_outlet, T_super, T_sub_cond, n_plates, frequency)
    results = {
        'T_evap': T_evap_guess,
        'T_source_in': T_source_inlet,
        'T_source_out': T_source_outlet,
        'F_source': (Q/(T_source_outlet-T_source_inlet)*4.2),
        'T_cond': T_cond_guess,
        'T_supply_in': T_supply_inlet,
        'T_supply_out': T_supply_outlet,
        'F_supply': (Q+P_e)/(T_supply_outlet-T_supply_inlet)*4.2,
        'COP': COP,
        'Q': Q+P_e,
        'Q_e': Q,
        'P_e': P_e,
        'freq': frequency,
        'm_ref': m,
        'I': I,
        'converged': converged,
        'iteration': iteration,
    }
    return as_html(results), results


if __name__ == '__main__':
    tic = time()
    T_evap_guess, T_cond_guess, COP, P, converged, iteration = heatpump_performance()
    toc = time()
    print("Run time: ", toc-tic)
    
    # Second run
    tic = time()
    T_evap_guess, T_cond_guess, COP, P, converged, iteration = heatpump_performance()
    toc = time()
    print("Run time: ", toc-tic)

    print(f"Converged: {converged} in {iteration} iterations taking {toc-tic:.2f} seconds")    
    print(f"Evaporator: {T_evap_guess:.2f} C")
    print(f"Condenser: {T_cond_guess:.2f} C")
    print(f"COP: {COP:.2f}")
    print(f"Power: {P:.2f} kW")