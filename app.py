from datetime import timedelta
from math import pi, sin

from numerous.sdk import JobClient, JobStatus

from scipy.special import factorial
import numpy as np
import matplotlib.pyplot as plt
import iterative_heatpump as heatpump

from report import generate_report

with JobClient.connect() as job:
    job.status = JobStatus.RUNNING
    job.message = "Getting parameters"
    job.progress = 0

    # Get parameters
    design_controls = job.scenario.components["design_controls"]
    
    n_plates = design_controls.parameters["n_plates"]
    frequency = design_controls.parameters["frequency"]
    
    operating_conditions = job.scenario.components["operating_conditions"]

    T_source_inlet = operating_conditions.parameters["T_source_inlet"]
    dT_source = operating_conditions.parameters["dT_source"]
    T_supply = operating_conditions.parameters["T_supply"]
    dT_supply = operating_conditions.parameters["dT_supply"]

    # log the input parameters
    job.log.info("n_plates: " + str(n_plates))
    job.log.info("frequency: " + str(frequency))
    job.log.info("T_source_inlet: " + str(T_source_inlet))
    job.log.info("dT_source" + str(dT_source))
    job.log.info("T_supply" + str(T_supply))
    job.log.info("dT_supply" + str(dT_supply))

    # Calculate
    job.message = "Calculating"

    html_str, results = heatpump.performance_as_html(
        T_source_inlet=T_source_inlet.value, 
        T_source_outlet=T_source_inlet.value - dT_source.value, 
        T_supply_inlet=T_supply.value-dT_supply.value,
        T_supply_outlet=T_supply.value, 
        n_plates=n_plates.value,
        frequency=frequency.value    
    )

    # Make report
    job.message = "Generating report"
    filename = "results"
    generate_report(html_str, filename)
    

    # Updload the report
    job.message = "Uploading report"
    job.file_manager.upload_path(filename+".html")
    
    job.progress = 100

    job.status = JobStatus.FINISHED
    job.message = "Done"