from pathlib import Path
import os
from numerous.html_report_generator  import Report, Section, Div

def generate_report(results_html:str, filename):
    # Define output folder and html file
    folder = Path('.')

    file = folder.joinpath(filename+'.html')

    #Remove previous report file if exists
    if os.path.exists(file):
        os.remove(file)

    # Create report
    report = Report(target_folder=folder, filename=filename)

    # Add info for the header and title page
    report.add_header_info(header='Numerous',
                        title='Heatpump Designer Report',
                        sub_title='',
                        sub_sub_title='',
                        footer_title='',
                        footer_content=''
                        )

    #Add a section
    section = Section(section_title="Results")

    section.add_content({
    'results': Div(results_html)
    })

    #Add the section to the report
    report.add_blocks({
        'section': section
    })

    # Save the report - creates the html output file
    report.save()


