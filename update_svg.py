import xml.etree.ElementTree as ET

def update_svg(elements, updates_dict):
    

    # Iterate through all elements and update values based on the provided dictionary
    for element in elements:
        element_id = element.get('id')
        #print(element_id)
        if element_id in updates_dict:
            element.text = updates_dict[element_id]
            # Find the <tspan> child
            #tspan = element.find('.//tspan', namespaces={'svg': 'http://www.w3.org/2000/svg'})
            #if tspan is not None:

            #    tspan.text = updates_dict[element_id]

def read_svg(svg_path):
    # Parse the SVG file
    tree = ET.parse(svg_path)
    root = tree.getroot()



    # Namespace handling for SVG files
    namespaces = {
        'xlink': 'http://www.w3.org/1999/xlink',
        'sodipodi': 'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
        'inkscape': 'http://www.inkscape.org/namespaces/inkscape',
        'svg': 'http://www.w3.org/2000/svg',
    }

    # Function to remove namespace
    def remove_namespace(element):
        if '}' in element.tag:
            element.tag = element.tag.split('}', 1)[1]  # Removes the namespace
        for child in element:
            remove_namespace(child)

    # Apply the function to all elements
    remove_namespace(root)
    
    
    for prefix, uri in namespaces.items():
        ET.register_namespace(prefix, uri)

    elements = root.findall('.//*[@id]', namespaces)

    return elements, root

def root_to_string(root):
    


    svg_str = ET.tostring(root, encoding='unicode')
    svg_str = svg_str.replace('{', '{').replace('}', '}')

    return svg_str

def get_file_path(filename):
    import os

    # Get the directory of the current file
    current_dir = os.path.dirname(__file__)

    # Name of another file in the same directory
    other_file_name = filename

    # Full path to the other file
    other_file_path = os.path.join(current_dir, other_file_name)

    return other_file_path

if __name__ == '__main__':
    updates = {'t_cond': '48.1', 't_evap': '0.1'}
    update_svg(read_svg('examples/marimo-heatpump/heatpump/display2.svg'), updates)
